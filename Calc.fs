let defDel = ';'
open System.Drawing
let negativeFilter arrayOfNums = 
    Array.filter (fun x -> x < 0) arrayOfNums
let lessThanThousand arrayOfNums = 
    Array.filter (fun x -> x <= 1000) arrayOfNums

let parseDel (s: string) = 
    let delStart = s.IndexOf('[') + 1 
    let delEnd = s.IndexOf(']')
    s.Substring(delStart, delEnd-delStart), s.Substring(delEnd+1)
let rec parseAllDell delString = 
    match delString with
    | "" -> []
    | _ -> let (del1, restOfString) = parseDel delString
           del1 :: parseAllDell restOfString
 
let (|WithDel|_|) (s: string) = 
    if s.StartsWith("//") then
        let indexOfnl = s.IndexOf('\n')
        let parsedDelimiter = s.Substring(2,indexOfnl-2) |> parseAllDell  
        Some(parsedDelimiter , s.Substring(indexOfnl+1))
    else 
        None

let addFunc (delimiter: char) (stringOfNums: string)  = 
    let arrayOfNums = stringOfNums.Split(delimiter,'\n')
    match arrayOfNums with
    | [|""|] -> 0
    | _ -> let arrayOfInt = arrayOfNums |> Array.map int
           let negNums = negativeFilter arrayOfInt
           if negNums.Length > 0 then
                invalidArg (sprintf "%A" negNums) (sprintf "Negative not Allowed")
           else
                lessThanThousand arrayOfInt
                |> Array.sum 
let Add (inputString: string) = 
    match inputString with
    | WithDel (delimiters , stringOfNums) -> let mutable strNums = stringOfNums
                                             for del in delimiters do 
                                                strNums <- strNums.Replace(del, (sprintf "%c" defDel))
                                             addFunc defDel strNums
    | _ -> addFunc defDel inputString 

Add "//[***][##][%]\n11%6***230##100"

